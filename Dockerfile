ARG IMAGE=debian:bookworm
FROM ${IMAGE}

RUN mkdir /build

RUN apt update
RUN apt -y dist-upgrade

RUN apt-get -y install coreutils quilt parted qemu-user-static debootstrap zerofree zip \
        dosfstools libarchive-tools libcap2-bin grep rsync xz-utils file git curl bc \
        qemu-utils kpartx gpg xxd kmod \
        git gettext-base

ARG CI_COMMIT_SHA=unknown
RUN apt update
RUN apt -y dist-upgrade



RUN git clone --depth 1 https://github.com/RPI-Distro/pi-gen.git /build/pi-gen
WORKDIR /build/pi-gen


